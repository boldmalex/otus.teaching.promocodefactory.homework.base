﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => x.ToModel()).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить роль по Id
        /// </summary>
        /// <returns>Роль</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name = "GetRole")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<RoleItemResponse>> GetRoleByIdAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            var roleModel = role.ToModel();

            return Ok(roleModel);
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="roleRequest">Данные новой роли</param>
        /// <returns>Новая роль</returns>
        /// <response code="201"> Возвращает новую роль</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(RoleItemResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<RoleItemResponse>> PostRoleAsync(RoleItemRequest roleRequest)
        {
            var newRole = roleRequest.ToDomain();

            // Добавляем роль
            var insertedRole = await _rolesRepository.InsertAsync(newRole);

            var insertedRoleModel = insertedRole.ToModel();

            return CreatedAtRoute("GetRole", new {id = insertedRoleModel.Id }, insertedRoleModel);
        }


        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <param name="id">ИД роли</param>
        /// <param name="roleRequest">Новые данные роли</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutRoleAsync(Guid id, RoleItemRequest roleRequest)
        {
            // Ищем роль
            var existingRole = await _rolesRepository.GetByIdAsync(id);

            if (existingRole == null)
            {
                return NotFound();
            }

            // Обновляем
            var newRole = roleRequest.ToDomain();
            await _rolesRepository.UpdateAsync(id, newRole);

            return Ok();
        }


        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteRoleAsync(Guid id)
        {
            // Ищем роль
            var existingRole = await _rolesRepository.GetByIdAsync(id);

            if (existingRole == null)
            {
                return NotFound();
            }

            // Удаляем
            await _rolesRepository.DeleteAsync(existingRole);

            return Ok();
        }
    }
}