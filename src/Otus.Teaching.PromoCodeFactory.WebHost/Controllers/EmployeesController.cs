﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IEmployeeRequestExtensionService _employeeRequestExtensionService;

        public EmployeesController(IRepository<Employee> employeeRepository,
                                   IEmployeeRequestExtensionService employeeRequestExtensionService)
        {
            _employeeRepository = employeeRepository;
            _employeeRequestExtensionService = employeeRequestExtensionService;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => x.ToShortModel()).ToList();

            return employeesModelList;
        }


        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns>Сотрудник</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name ="GetEmployee")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = employee.ToModel();

            return Ok(employeeModel);
        }


        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeRequest">Данные нового сотрудника</param>
        /// <returns>Новый сотрудник</returns>
        /// <response code="201"> Возвращает нового сторудника</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<EmployeeResponse>> PostEmployeeAsync(EmployeeRequest employeeRequest)
        {
            var newEmployee = await _employeeRequestExtensionService.PrepareDomain(employeeRequest);

            // Доавляем сотрудника
            var insertedEmployee = await _employeeRepository.InsertAsync(newEmployee);

            var insertedEmpolyeeModel = insertedEmployee.ToModel();

            return CreatedAtRoute("GetEmployee", new { id = insertedEmpolyeeModel.Id }, insertedEmpolyeeModel);
        }


        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="id">ИД сотрудника</param>
        /// <param name="employeeRequest">Новые данные сотрудника</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            // Ищем сотрудника
            var existingEmployee = await _employeeRepository.GetByIdAsync(id);

            if (existingEmployee == null)
            {
                return NotFound();
            }

            // Обновляем
            var newEmpolyee = await _employeeRequestExtensionService.PrepareDomain(employeeRequest);
            await _employeeRepository.UpdateAsync(id, newEmpolyee);

            return Ok();
        }


        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            // Ищем сотрудника
            var existingEmployee = await _employeeRepository.GetByIdAsync(id);

            if (existingEmployee == null)
            {
                return NotFound();
            }

            // Удаляем
            await _employeeRepository.DeleteAsync(existingEmployee);

            return Ok();
        }
    }
}