﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class EmpoyeeExtensions
    {
        public static EmployeeResponse ToModel(this Employee employee)
        {
            if (employee == null)
                throw new ArgumentNullException(nameof(employee));

            return new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = employee.Roles.Select(x => x.ToModel()).ToList()
            };
        }

        public static EmployeeShortResponse ToShortModel(this Employee employee)
        {
            if (employee == null)
                throw new ArgumentNullException(nameof(employee));

            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName,
            };
        }
    }
}
