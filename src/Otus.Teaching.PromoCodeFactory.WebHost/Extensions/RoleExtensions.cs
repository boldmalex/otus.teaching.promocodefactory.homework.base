﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class RoleExtensions
    {
        public static Role ToDomain(this RoleItemRequest roleItemRequest)
        {
            if (roleItemRequest == null)
                throw new ArgumentNullException(nameof(roleItemRequest));

            return new Role()
            {
                Name = roleItemRequest.Name,
                Description = roleItemRequest.Description
            };
        }

        public static RoleItemResponse ToModel(this Role role)
        {
            if (role == null)
                throw new ArgumentNullException(nameof(role));

            return new RoleItemResponse()
            {
                Id = role.Id,
                Name = role.Name,
                Description = role.Description
            };
        }
    }
}
