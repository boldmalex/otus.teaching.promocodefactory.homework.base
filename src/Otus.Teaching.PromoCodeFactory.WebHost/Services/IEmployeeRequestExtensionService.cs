﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface IEmployeeRequestExtensionService
    {
        Task<Employee> PrepareDomain(EmployeeRequest employeeRequest);
        Task<List<Employee>> PrepareDomain(List<EmployeeRequest> employeeRequest);
    }
}
