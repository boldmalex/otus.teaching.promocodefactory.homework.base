﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class EmployeeRequestExtensionService : IEmployeeRequestExtensionService
    {
        private readonly IRepository<Role> _rolesRepository;

        public EmployeeRequestExtensionService(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public Task<Employee> PrepareDomain(EmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                throw new ArgumentNullException(nameof(employeeRequest));

            List<Role> roles = new List<Role>();

            employeeRequest.RoleIds.ForEach(async x => roles.Add(await _rolesRepository.GetByIdAsync(x)));

            Employee result = new Employee()
            {
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                Roles = roles,
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };

            return Task.FromResult(result);
        }


        public Task<List<Employee>> PrepareDomain(List<EmployeeRequest> employeeRequest)
        {
            if (employeeRequest == null)
                throw new ArgumentNullException(nameof(employeeRequest));

            List<Employee> result = new List<Employee>();
            
            employeeRequest.ForEach(async x => result.Add(await PrepareDomain(x)));

            return Task.FromResult(result);
        }

    }
}
