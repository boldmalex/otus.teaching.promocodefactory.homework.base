﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> InsertAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return Task.FromResult(entity);
        }

        public async Task<bool> UpdateAsync(Guid id, T entity)
        {
            bool result = false;

            var existingEntity = await GetByIdAsync(id);

            if (existingEntity != null)
            {
                var entityIndex = Data.IndexOf(existingEntity);
                entity.Id = id;
                Data[entityIndex] = entity;
                result = true;
            }

            return result;
        }

        public async Task<bool> DeleteAsync(T entity)
        {
            bool result = false;

            var existingEntity = await GetByIdAsync(entity.Id);

            if (existingEntity != null)
            {
                Data.Remove(existingEntity);
                result = true;
            }

            return result;
        }
    }
}